class SlidesshowsController < ApplicationController
  def show
  @slideshow = Slideshow.find(params[:id])
  session[:slideshow] = @slideshow
  session[:slide_index] = 0
  @slide = @slideshow.slides[0]
  end
end